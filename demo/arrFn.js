// Modo clásico para declarar una función

function funcionclasica(numero) {
    console.log(numero * 2)
}

var funcion2 = function (numero) {
    console.log(numero * 2)
}


// Arrow function

var arrowFn = (numero) => {
    console.log(numero * 2)
}
